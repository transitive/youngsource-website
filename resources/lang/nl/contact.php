<?php

return [
    'title' => 'Contacteer ons',
    'address' => [
        'title' => 'Ons kantoor',
        'name' => 'VIVES Innovation Centre',
        'line1' => 'Etienne Sabbelaan 4',
        'line2' => '8500 Kortrijk',
        'line3' => 'West-Vlaanderen, België',
    ],
    'communications' => [
        'title' => 'Contact',
        'telephone' => 'telefoon',
        'number' => '+32 485 49 64 90',
        'email' => 'email',
        'email_address' => 'contact@youngsource.be',
    ],
    'po' => [
        'title' => 'Onze postbus',
        'line1' => 'Rijsenbergstraat 406',
        'line2' => '9000 Gent',
        'line3' => 'Oost-Vlaanderen, België',
    ],
];
<?php

return [
    'title' => 'Onze oplossingen',
    'website' => [
        'idea' => 'Uw online aanwezigheid',
        'description' => 'Een simpele en handige website waarmee u voet aan de grond zet op het internet.',
        'pricing' => 'De eerste verkennende meeting is gratis. Voor de verdere ontwikkeling rekenen we een uurloon van 45 euro.',
    ],
    'solutions' => [
        'idea' => 'Een oplossing voor de complexiteit',
        'description' => 'Wanneer uw specifieke noden beter verdienen dan een commercieel softwarepakket, ontwikkelen we een oplossing die volledig is toegesneden op uw wensen, ontwikkeld door onze experts.',
        'pricing' => 'De eerste verkennende meeting is gratis. De kostprijs hangt af van de omvang en complexiteit van de oplossing.',
    ],
    'proof_of_concept' => [
        'idea' => 'Ontwikkelen van een proof of concept',
        'description' => 'Heeft u een idee en wil u een proof of concept uitwerken? We werken samen met u om snel en efficiënt een oplossing op te leveren. Onze studenten zullen de gelegenheid gretig te baat nemen om nieuwe technologieën te leren onder onze supervisie.',
        'pricing' => 'De eerste verkennende meeting is gratis. Voor de verdere ontwikkeling rekenen we een uurloon van 35 euro.',
    ],
];

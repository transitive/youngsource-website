<?php

return [
    'title' => 'Ontmoet het team',
    'title2' => 'Het kernteam',
    'maximilian' => [
        'name' => 'Maximilian Coutuer',
        'expertise1' => 'Communication Expert',
        'expertise2' => 'User Experience & Design',
        'pitch' => 'Na een master in communicatiewetenschappen en een rijke ervaring met user experience design tijdens mijn loopbaan realiseerde ik me dat IT mijn ware passie is. Communicatie is de sleutel tot elk geslaagd softwareproject.',
    ],
    'ghlen' => [
        'name' => 'Ghlen Nagels',
        'expertise1' => 'System Analyst',
        'expertise2' => 'Customer Relations',
        'pitch' => 'Met mijn ervaring in systeemontwikkeling en passie voor oplossingen ontwerp en bouw ik de fundamenten van onze projecten. Vanuit deze positie overleg ik met onze klanten over het project en de implementatie, planning en oplevering ervan.',
    ],
    'rian' => [
        'name' => 'Rian Velders',
        'expertise1' => 'Creative Adviser',
        'expertise2' => 'Coach',
        'pitch' => 'Met mijn ervaring in industrieel design ben ik de creatieve drijfveer van Youngsource. Ik ontwerp en implementeer alle visuele elementen, denk buiten het kader en sta in voor de creatieve aspecten van elk project.',
    ],

    'titles' => [
        'expertise' => 'Expertise',
        'contact' => 'Contacteer'
    ]
];

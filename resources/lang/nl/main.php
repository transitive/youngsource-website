<?php
return [
    'title' => 'youngsource',
    'subtitle' => 'simple solutions to complex problems',
    'welcome' => [
        'title' => 'welkom',
    ],
    'mission' => [
        'title' => 'missie',
    ],
    'solutions' => [
        'title' => 'oplossingen',
    ],
    'meet_us' => [
        'title' => 'ontmoet ons',
    ],
    'contact' => [
        'title' => 'contacteer ons',
    ]
];
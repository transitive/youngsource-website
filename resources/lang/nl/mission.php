<?php


return [
    'intro' =>  [
        'title' => 'Software voor een wereld in beweging',
        'content' => 'We zijn een jeugdig en dynamisch team die een frisse nieuwe kijk bieden op de wereld van software. We beperken ons niet tot de standaardprodukten van bekende leveranciers, maar leveren een nieuwe en unieke benadering. We leggen de nadruk op de schaalbaarheid, gebruikerservaring, eenvoud en vooral de onderhoudbaarheid van onze oplossingen.',
    ],
    'communication' => [
        'title' => 'Open communicatie',
        'content' => 'Een geslaagd softwareproject begint bij klare communicatie over noden en verwachtingen. We nemen dan ook de tijd om naar uw wensen te luisteren en uw uitdagingen goed te begrijpen. We spreken uw taal en communiceren helder en duidelijk.',
    ],
    'code' => [
        'title' => 'Open code',
        'content' => 'Onze oplossing wordt uw product. U wordt eigenaar van alle geschreven code en andere componenten, en dit reeds vanaf de eerste letter code. Zo kan u later zelf wijzigingen doorvoeren of een andere aanbieder in dienst nemen om de software aan te passen aan toekomstige noden.',
    ],
    'technologies' => [
        'title' => 'Gebouwd op rotsvaste technologieën',
        'content' => 'Software heeft geen houdbaarheidsdatum. Onze oplossingen maken gebruik van open technologieën die niet afhankelijk zijn van de grillen van een commercieel bedrijf in een ver land. Hierdoor zal u als klant zonder problemen ondersteuning kunnen vinden en upgrades doorvoeren voor de volledige levensduur van uw toepassing.'
    ],
    'pricing' => [
        'title' => 'Voor elk budget een oplossing',
        'content' => 'Aarzel niet om contact met ons op te nemen. We zullen samen met u uw noden onderzoeken, gratis en voor niks. U betaalt enkel voor de daadwerkelijk geschreven code.',
    ]
];

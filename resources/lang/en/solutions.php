<?php

return [
    'title' => 'Our solutions',
    'website' => [
        'idea' => 'Your online presence',
        'description' => 'A basic website aimed at establishing your online presence.',
        'pricing' => 'Our first exploratory meeting is free. For further development we charge an hourly wage of 45 euros.',
    ],
    'solutions' => [
        'idea' => 'Solving the complexity',
        'description' => 'Where your specific needs deserve better than a commercial software solution, we develop your own custom built solution designed for your own needs and wants, designed by our experts.',
        'pricing' => 'Our first exploratory meeting is free. Pricing depends on the scale and complexity of the challenge.',
    ],
    'proof_of_concept' => [
        'idea' => 'Developing a proof of concept',
        'description' => 'Do you have an idea you would like to see come to life as a proof of concept? We work with you to quickly and efficiently bring your vision to fruition. Our students will eagerly develop your project and build experience with new technologies under our expert supervision.',
        'pricing' => 'Our first exploratory meeting is free. For further development we charge an hourly wage of 35 euros.',
    ],
];

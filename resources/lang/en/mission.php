<?php

return [
    'intro' =>  [
        'title' => 'Software for a world in motion',
        'content' => 'We are a young and dynamic team with a fresh view on the world of software. We do not restrict ourselves to standard solutions by well known providers, but bring a unique and brand new approach. We emphasize scaleability, a solid user experience, simplicity and pay particular attention to maintainability.',
    ],
    'communication' => [
        'title' => 'Open communication',
        'content' => 'The success of a software project depends on clear communication about needs and expectations. We take our time to listen to your wishes and gain an understanding of your challenges. We speak the language of business.',
    ],
    'code' => [
        'title' => 'Open code',
        'content' => 'Our solution will become yours. You will be the owner of all written code and other assets, starting with the first letter of code. This ensures that you will be able to change or update the application at a later point in time to accomodate future needs.',
    ],
    'technologies' => [
        'title' => 'Built on a bedrock of proven technologies',
        'content' => 'Software should not have an expiry date. Our solutions make use of open technologies that are independent of the fickle policies of a commercial provider in a distant country. This ensures you will be able to find support and conduct upgrades for the entire useful lifespan of your application.'
    ],
    'pricing' => [
        'title' => 'A solution for any budget',
        'content' => 'Do not hesitate to contact us. We will analyze your needs at no cost; you only pay for the code that is actually written.',
    ]
];

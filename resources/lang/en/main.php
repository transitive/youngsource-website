<?php
return [
    'title' => 'youngsource',
    'subtitle' => 'simple solutions to complex problems',
    'welcome' => [
        'title' => 'welcome',
    ],
    'mission' => [
        'title' => 'mission',
    ],
    'solutions' => [
        'title' => 'solutions',
    ],
    'meet_us' => [
        'title' => 'meet us',
    ],
    'contact' => [
        'title' => 'contact',
    ]
];
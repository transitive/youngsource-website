<?php

return [
    'title' => 'Contact us',
    'address' => [
        'title' => 'Our office',
        'name' => 'VIVES Innovation Centre',
        'line1' => 'Etienne Sabbelaan 4',
        'line2' => '8500 Kortrijk',
        'line3' => 'West-Flanders, Belgium',
    ],
    'communications' => [
        'title' => 'Contact',
        'telephone' => 'telephone',
        'number' => '+32 485 49 64 90',
        'email' => 'email',
        'email_address' => 'contact@youngsource.be',
    ],
    'po' => [
        'title' => 'Our PO Box',
        'line1' => 'Rijsenbergstraat 406',
        'line2' => '9000 Ghent',
        'line3' => 'East-Flanders, Belgium',
    ],
];
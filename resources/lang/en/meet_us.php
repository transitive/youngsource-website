<?php

return [
    'title' => 'Meet the team',
    'title2' => 'The core team',
    'maximilian' => [
        'name' => 'Maximilian Coutuer',
        'expertise1' => 'Communication Expert',
        'expertise2' => 'User Experience & Design',
        'function' => 'Founder & Front-End',
        'pitch' => 'After a master`s degree in communications and a wealth of experience in user centric design during my career, I realized that my true passion lies in IT. Communication is the key to any successful software project.',
    ],
    'ghlen' => [
        'name' => 'Ghlen Nagels',
        'expertise1' => 'System Analyst',
        'expertise2' => 'Customer Relations',
        'function' => 'CEO & Fullstack dev',
        'pitch' => 'Thanks to my experience with systems development and passion for solutions, I design and build the foundations of our projects. This puts me in the ideal position to keep in touch with our customers and discuss planning, implementation and delivery details.',
    ],
    'rian' => [
        'name' => 'Rian Velders',
        'expertise1' => 'Creative Adviser',
        'expertise2' => 'Coach',
        'function' => 'CCO & Strategist',
        'pitch' => 'My experience in industrial design allows me to be the creative heart of Youngsource. I develop and implement all visual elements, think outside the box and am responsible for the creative aspects of any project.',
    ],

    'titles' => [
        'expertise' => 'Expertise',
        'contact' => 'Contact us'
    ]
];

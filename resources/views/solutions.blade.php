<div class="container-fluid  p-0 min-h-100 solutions-cloud nav-offset" id="solutions">
    <div class="container justify-content-center flex-md-column flex-row">
        <div class="row d-flex">
            <div class="col-12">
                <h2 class="text-center">{{__('solutions.title')}}</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="card-group vh-25 vh-md-50 justify-content-center">
                @include('partials._card', [
                    'description' => __('solutions.proof_of_concept.description'),
                    'pricing' => __('solutions.proof_of_concept.pricing'),
                    'title' => __('solutions.proof_of_concept.idea'),
                    'img' => asset('images/proof of concept 1.svg'),
                ])
                @include('partials._card', [
                     'description' => __('solutions.website.description'),
                     'pricing' => __('solutions.website.pricing'),
                     'title' => __('solutions.website.idea'),
                     'img' => asset('images/3online presence.svg'),
                 ])
                <div class="w-100 d-none d-sm-block d-md-block d-lg-none"><!-- wrap every 2 on md--></div>
                <div class="w-25 justify-content-center d-none d-sm-block d-md-block d-lg-none">
                    <!-- wrap 1 on md--></div>

                @include('partials._card', [
                   'description' => __('solutions.solutions.description'),
                   'pricing' => __('solutions.solutions.pricing'),
                   'title' => __('solutions.solutions.idea'),
                   'img' => asset('images/guide trough complexity.svg'),
               ])
                <div class="w-25 justify-content-center d-none d-sm-block d-md-block d-lg-none">
                    <!-- wrap 1 on md--></div>
                <div class="w-100 d-none d-sm-none d-md-none d-lg-block"><!-- wrap every 3 on lg--></div>
            </div>
        </div>
    </div>
</div>

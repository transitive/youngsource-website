<nav class="navbar navbar-expand-md pt-1 pb-1 navbar-fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#home"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <!-- <img class="hamburger" src="/images/hamburger.ico"/>-->
            <span class="fa fa-bars text-white"> </span>
        </button>
        <div class="collapse navbar-collapse navbar-fixed-top-mobile" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#intro">{{__('main.welcome.title')}} <span
                                class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#mission">{{__('main.mission.title')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#solutions">{{__('main.solutions.title')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#who">{{__('main.meet_us.title')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">{{__('main.contact.title')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/nl">NL</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/en">EN</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

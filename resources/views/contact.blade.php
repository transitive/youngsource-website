<div class="container-fluid p-0 vh-100 nav-offset" id="contact">


    <div class="row vh-100">
        <div class="col-12 col-md-6 mb-3 mb-md-0" id="map">
            <iframe src="https://snazzymaps.com/embed/81424" width="100%" height="100%"
                    style="border:none;"></iframe>
        </div>
        <div class="col-12  col-md-6  d-md-flex justify-content-center flex-md-column">
            <div class="row  justify-content-center">
                <h2 class="text-center">{{__('contact.title')}}</h2>
            </div>
            <div class="row  justify-content-center">
                <h5 class="text-center">{{__('contact.address.title')}}</h5>
            </div>

            <div class="row  justify-content-center">
                <address class="text-center">{{__('contact.address.name')}}<br/>{{__('contact.address.line1')}}
                    <br/>{{__('contact.address.line2')}}<br/>{{__('contact.address.line3')}}<br/></address>
            </div>


            <div class="row  justify-content-center">
                <h5 class="text-center">{{__('contact.po.title')}}</h5>
            </div>

            <div class="row  justify-content-center">
                <address class="text-center">{{__('contact.po.line1')}}<br/>{{__('contact.po.line2')}}
                    <br/>{{__('contact.po.line3')}}<br/></address>
            </div>

            <div class="row  justify-content-center">
                <h5 class="text-center">{{__('contact.communications.title')}}</h5>
            </div>

            <div class="row  justify-content-center">
                <address class="text-center">{{__('contact.communications.telephone')}}
                    : {{__('contact.communications.number')}}<br/>{{__('contact.communications.email')}}: <a
                            href="mailto:{{__('contact.communications.email_address')}}">{{__('contact.communications.email_address')}}</a><br/>
                </address>
            </div>

        </div>
    </div>


</div
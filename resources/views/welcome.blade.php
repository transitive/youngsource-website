<!doctype html>
<html lang="{{ app()->getLocale() }}">

@include('head')

<body>
<div class="p-0 container-fluid main-wrapper">

    @include('cookieConsent::index')
    @include('eyecatcher')

    @include('intro')

    @include('communication')


    @include('code')

    {{--solutions--}}
    @include('solutions')


    {{--Wie zijn we--}}
    @include('meet_us')

    {{--contact--}}
    @include('contact')>


    <footer class="container-fluid gradient-footer p-0">

    </footer>


</div>

</body>
</html>

@extends('layouts.double_container')

@section('background')
    <div class="row p-0 m-0">
        <div class="col-md-6 col-12 intro-rock">
            <img src="{{ asset('images/mountain.png') }}">
        </div>
        <div class="col-md-6 col-12">
            <div class="w-100 h-100"></div>
        </div>
    </div>
@overwrite

@section('foreground')
    <div class="row vh-100 align-items-center justify-content-center" >
        <div class="col-md-6 col-12 vh-100">

        </div>
        <div class="col-md-6 col-12 nav-offset" id="mission">
            <h2 class="text-center">{{__('mission.communication.title')}}</h2>
            <p class="text-justify">{{__('mission.communication.content')}}</p>
            <h2 class="text-center">{{__('mission.pricing.title')}}</h2>
            <p class="text-justify">{{__('mission.pricing.content')}}</p>
        </div>
    </div>
@overwrite


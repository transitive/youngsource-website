<div class="container-fluid vh-100 nav-offset" id="intro">
    <div class="container">
        <div class="row vh-100 align-items-center justify-content-center">
            <div class="col-md-6 col-12">
                <h2 class="text-center">{{__('mission.intro.title')}}</h2>
                <p class="text-justify">{{__('mission.intro.content')}}</p>
            </div>
            <div class="col-md-6 col-12 vh-100 {{--d-none d-md-block--}} intro-book">

            </div>
        </div>
    </div>
</div>

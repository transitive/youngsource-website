<div class="card mr-1 ml-1">
    <img class="card-img-top h-40" src="{{ $img }}"
         alt="Card image cap">
    <div class="card-body text-center d-flex flex-column">
        <h5 class="card-title">{{ $title}}</h5>
        <p class="card-text">{{ $description }}</p>
        <div class="mt-auto">
            <p class="card-text text-muted">{{$pricing }}</p>
        </div>
    </div>
</div>

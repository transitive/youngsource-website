<div class="card mb-4">
    <img class="card-img-top img-fluid" src="{{$img}}" alt="Card image cap">
    <div class=" text-center card-body  d-flex flex-column">
        <h5 class="card-title">{{$name}}</h5>
        <p class="card-text ">{{$pitch}}</p>
        <div class="mt-auto">
            <h5 class="card-title">{{$title_expertise}}</h5>
            <ul class="list-group list-group-flush mb-1">
                <li class="list-group-item">{{$text1_expertise}}</li>
                <li class="list-group-item">{{$text2_expertise}}</li>
            </ul>


          {{--  <h5 class="card-title">{{$function}}</h5>--}}
            <h5 class="card-title">{{$title_contact}}</h5>
            <a href="mailto:{{$mailto}}"> {{--<span class="fa fa-send"></span>--}}<img class="img-fluid mr-2"
                                                              src="/images/mail.png"
                                                              alt="mail"/></a>
            <a href="{{$linkedin}}" >{{-- <span class="fa fa-linkedin-square"></span>--}}<img
                        class="img-fluid"
                        src="/images/linkedin.png"
                        alt="linkedin"/></a>
        </div>
    </div>
</div>

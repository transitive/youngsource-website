@extends('layouts.double_container')

@section('foreground')
    <div class="row vh-100 align-items-center justify-content-center">
        <div class="col-md-6 col-12">
            <h2 class="text-center">{{__('mission.code.title')}}</h2>
            <p class="text-justify">{{__('mission.code.content')}}</p>
            <h2 class="text-center">{{__('mission.technologies.title')}}</h2>
            <p class="text-justify">{{__('mission.technologies.content')}}</p>
        </div>
        <div class="col-md-6 col-12">

        </div>
    </div>
@overwrite

@section('background')
    <div class="row p-0 m-0">
        <div class="col-md-6 col-12 vh-100">
            <div class="w-100 h-100"></div>
        </div>
        <div class="col-md-6 col-12 intro-gear">
            <img src="{{ asset('images/gear.png') }}"/>
        </div>
    </div>
@overwrite



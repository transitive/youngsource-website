<div class="container-fluid p-0 h-100 gradient-heading" id="home">

   @include('nav')

    <div class="row h-100 mr-0 ml-0 pl-0 pr-0 pl-sm-5 pr-sm-5 pl-lg-0 pr-lg-0  vw-lg-50 vw-100 mr-lg-auto ml-lg-auto align-items-center justify-content-center">
        <div class="col-12">
            <img class="img-fluid eyecatcher-logo" src="{{ asset('images/longlogo.png') }}"/>
            <h2 class="text-center eyecatcher-slogan text-white ">{{ __('main.subtitle') }}</h2>
        </div>
    </div>
    <div class="eyecatcher-waves">
        <img class="wave-back" src="{{ asset('images/golf_back.png') }}"/>
        <img class="wave-mid" src="{{ asset('images/golf_mid.png') }}"/>
        <img class="wave-front" src="{{ asset('images/golf_front.png') }}"/>
    </div>
</div>

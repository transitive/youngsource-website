<div  class="container-fluid position-relative vh-100">
    <div class="container-fluid vh-100 position-absolute" style="top:0; left:0;">
        @yield('background')
    </div>
    <div class="container vh-100 position-absolute" style="left: 50%; transform: translate(-50%, 0);">
        @yield('foreground')
    </div>
</div>
<div class="vh-100 d-md-none ">

</div>
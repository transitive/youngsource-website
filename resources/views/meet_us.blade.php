<div class="container-fluid  p-0 min-h-100 d-md-flex justify-content-center flex-md-column  who-cloud  nav-offset"
     id="who">


    <div class="container">

        <div class="row justify-content-center who-foto mb-2 align-items-center">
            <h2 class="text-center text-white">{{__('meet_us.title')}}</h2>
        </div>

        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card-deck justify-content-center">
                    @include('partials.meet_us_card',[
                    'function' => __('meet_us.ghlen.function'),
                            'img' => asset('images/ghlen.png'),
                            'name' => __('meet_us.ghlen.name'),
                            'pitch' => __('meet_us.ghlen.pitch'),
                            'title_expertise' => __('meet_us.titles.expertise'),
                            'text1_expertise'=>__('meet_us.ghlen.expertise1'),
                            'text2_expertise'=>__('meet_us.ghlen.expertise2'),
                            'title_contact' =>__('meet_us.titles.contact'),
                            'mailto' => 'ghlen.nagels@youngsource.be',
                            'linkedin' => 'https://www.linkedin.com/in/ghlen-nagels-1b6663134/',
                    ])


                    @include('partials.meet_us_card',[
                                 'function' => __('meet_us.maximilian.function'),
                                'img' => asset('images/max.png'),
                                'name' => __('meet_us.maximilian.name'),
                                'pitch' => __('meet_us.maximilian.pitch'),
                                'title_expertise' => __('meet_us.titles.expertise'),
                                'text1_expertise'=>__('meet_us.maximilian.expertise1'),
                                'text2_expertise'=>__('meet_us.maximilian.expertise2'),
                                'title_contact' =>__('meet_us.titles.contact'),
                                'mailto' => 'maximilian.coutuer@youngsource.be',
                                'linkedin' => 'https://www.linkedin.com/in/maximilian-coutuer-0ba4a517/',
                    ])







                    <div class="w-100 d-block d-sm-block d-md-block d-lg-none"><!-- wrap every 2 on md--></div>
                    <div class="w-25 justify-content-center d-block d-sm-block d-md-block d-lg-none">
                        <!-- wrap 1 on md--></div>

                    @include('partials.meet_us_card',[
                                'function' => __('meet_us.rian.function'),
                                'img' => asset('images/rianbw.png'),
                                'name' => __('meet_us.rian.name'),
                                'pitch' => __('meet_us.rian.pitch'),
                                'title_expertise' => __('meet_us.titles.expertise'),
                                'text1_expertise'=>__('meet_us.rian.expertise1'),
                                'text2_expertise'=>__('meet_us.rian.expertise2'),
                                'title_contact' =>__('meet_us.titles.contact'),
                                'mailto' => 'rian.velders@youngsource.be',
                                'linkedin' => 'https://www.linkedin.com/in/rian-velders-05a5889b/',
     ])
                    <div class="w-25 justify-content-center d-block d-sm-block d-md-block d-lg-none">
                        <!-- wrap 1 on md--></div>
                    <div class="w-100 d-none d-sm-none d-md-none d-lg-block"><!-- wrap every 3 on lg--></div>
                </div>
            </div>
        </div>
    </div>
</div>

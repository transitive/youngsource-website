/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import $ from 'jquery/src/jquery'

$(() => {
    $(document).on('click', '.navbar-collapse a', function (e) {
        if ($(e.target).is('a:not(".dropdown-toggle")')) {
            $(".collapse").removeClass("show");
        }
    });

    $(".main-wrapper").scroll(function () {
        let $nav = $(".navbar-fixed-top");
        let $home = $("#home");

        $nav.toggleClass('navbar-scrolled', $(".main-wrapper").scrollTop() + 100 > $home.height() - (2 * $nav.height()));
    });

    require.context('../img', true, /\.(png|jpe?g|gif)$/);
});

<?php
/**
 * Created by PhpStorm.
 * User: arnel
 * Date: 4/07/2018
 * Time: 9:29
 */

namespace App\Http\Controllers\Welcome;


use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{


    /**
     * Show the view "welcome"
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return view("welcome");
    }

}